import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify';
import router from './router'
import parse from './parse'
import './registerServiceWorker'
import './assets/style/global.sass'

const toBase64 = file => new Promise((resolve, reject) => {
  const reader = new FileReader();
  reader.readAsDataURL(file);
  reader.onload = () => resolve(reader.result);
  reader.onerror = error => reject(error);
});

Vue.config.productionTip = false
Vue.prototype.$parse = parse
Vue.prototype.$toBase64 = toBase64

new Vue({
  vuetify,
  router,
  render: h => h(App)
}).$mount('#app')
