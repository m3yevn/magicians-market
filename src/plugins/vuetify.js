import Vue from 'vue';
import Vuetify from 'vuetify/lib';
import colors from 'vuetify/lib/util/colors'

Vue.use(Vuetify);

export default new Vuetify({
  theme:{
    themes:{
      light:{
        primary: colors.red.darken4, 
        secondary: colors.amber.accent2, 
        accent: colors.grey.darken4, 
      }
    },
    options: {
      customProperties: true,
    }
  },
  icons: {
    iconfont: 'mdi',
  },
});
