import Vue from 'vue'
import Router from 'vue-router'
import Parse from './parse'

Vue.use(Router);

const authGuard = (to,_,next) => {
    const user = Parse.User.current();
    if(user === null){
        next({path:'/login'})
    }
    else{
        next();
    }
}

const loggedGuard = (to,_,next) => {
    const user = Parse.User.current();
    if(user !== null){
        next({path:'/'})
    }
    else{
        next();
    }
}


export default new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        {
            path: '/',
            component: () => import('./views/Home.vue'),
            beforeEnter: authGuard,
            children:[
                {
                    path:'/',
                    name:'marketplace',
                    component: () => import('./views/Marketplace.vue')
                },
                {
                    path:'/categories',
                    name:'categories',
                    component: () => import('./views/Category.vue')
                },
                {
                    path:'/outlet-locations',
                    name:'locations',
                    component: () => import('./views/Outlet.vue')
                },
                {
                    path:'/help',
                    name:'help',
                    component: () => import('./views/Help.vue')
                },
                {
                    path:'/submit-feedback',
                    name:'feedback',
                    component: () => import('./views/Feedback.vue')
                },
                {
                    path:'/settings',
                    name:'settings',
                    component: () => import('./views/Setting.vue')
                },
                {
                    path:'/about',
                    name:'about',
                    component: () => import('./views/About.vue')
                },
                {
                    path:'/personal-info',
                    name:'personalinfo',
                    component: () => import('./views/PersonalInfo.vue')
                },
                {
                    path:'/shopping-cart',
                    name:'shoppingcart',
                    component: () => import('./views/ShoppingCart.vue')
                },
                {
                    path:'/order-history',
                    name:'orderhistory',
                    component: () => import('./views/OrderHistory.vue')
                },
                {
                    path:'/billing-info',
                    name:'billinginfo',
                    component: () => import('./views/BillingInfo.vue')
                },
                {
                    path:'/notifications',
                    name:'notifications',
                    component: () => import('./views/Notification.vue')
                }
            ]
        },
        {
            path: '/login',
            name: 'login',
            component: () => import('./views/Login.vue'),
            beforeEnter: loggedGuard
        },
        {
            path: '/register',
            name: 'register',
            component: () => import('./views/Register.vue'),
            beforeEnter: loggedGuard
        },
        { 
            path: "*", 
            component: () => import('./views/NotFound.vue') }
    ]
})