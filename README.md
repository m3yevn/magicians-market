# Magicians' Market 🧙

This is a Proof-of-concept (POC) application for a marketplace to purchase magic props.

## Built with 🏗️

 - VueJS
 - Vuetify
 - Back4App

## Docker Hub 🐳

Available at [https://hub.docker.com/repository/docker/m3yevn/magicians-market](https://hub.docker.com/repository/docker/m3yevn/magicians-market)


## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
